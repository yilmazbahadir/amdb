CREATE TABLE activity 
( 
  id BIGINT NOT NULL 
, planStartDate DATETIME NULL 
, estimated BIGINT NULL COMMENT 'estimated(in minitues)=planEndDate - planStartDate' 
, timeSpent BIGINT NULL 
, planEndDate DATETIME NULL 
, title VARCHAR(255) NULL 
, descr TEXT NULL 
, resolution INT NULL COMMENT '1=realized,
2=partially_realized,
3=not_realized' 
, assignee BIGINT NULL 
, assigner BIGINT NULL 
, version VARCHAR(50) NULL 
, entityType VARCHAR(50) NULL COMMENT 'customer veya meeting ' 
, entityRefId BIGINT NULL COMMENT 'ilgili tipin referans id si' 
, entityDisplayName VARCHAR(255) NULL 
, activityType VARCHAR(50) NULL COMMENT 'Visit, Meeting, Task, NonWoking' 
, activitySubType VARCHAR(50) NULL COMMENT 'Project, Periodical, Independet, Customer' 
, parent BIGINT NULL 
, parentOrder INT NULL 
, topParent BIGINT NULL 
, important INT NULL 
, activityStatus VARCHAR(255) NULL COMMENT '1=open,
2=reopened,
3=closed' 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE activityLog 
( 
  id BIGINT NOT NULL 
, descr TEXT NULL 
, important INT NULL 
, activity BIGINT NULL 
, logDate DATETIME NULL 
, logger BIGINT NULL 
, activityLogStatus INT NULL COMMENT '1=realized,
2=not_realized' 
, timeSpent INT NULL COMMENT 'in minutes' 
, reason INT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE association 
( 
  id BIGINT NOT NULL 
, entityType VARCHAR(50) NULL 
, entityRefId BIGINT NULL 
, targetEntityType VARCHAR(50) NULL 
, targetEntityRefId BIGINT NULL 
, association VARCHAR(50) NOT NULL COMMENT 'participates, likes' 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE comment 
( 
  id BIGINT NOT NULL 
, activity BIGINT NULL 
, descr VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE customer 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NULL 
, tel VARCHAR(20) NULL 
, fax VARCHAR(20) NULL 
, email VARCHAR(100) NULL 
, class VARCHAR(255) NULL 
, sector VARCHAR(255) NULL 
, type INT NOT NULL DEFAULT 1 
, address VARCHAR(255) NULL 
, district VARCHAR(255) NULL 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE customerContact 
( 
  id BIGINT NOT NULL 
, customer BIGINT NOT NULL 
, firstName VARCHAR(255) NOT NULL 
, lastName VARCHAR(255) NOT NULL 
, jobTitle VARCHAR(255) NULL 
, tel VARCHAR(255) NULL 
, fax VARCHAR(255) NULL 
, gsm VARCHAR(255) NULL 
, email VARCHAR(255) NULL 
, locationId INT NULL 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, primaryContact INT NULL DEFAULT 0 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE department 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NULL 
, firm BIGINT NOT NULL 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE fileAttachment 
( 
  id BIGINT NOT NULL 
, entityType VARCHAR(50) NULL 
, entityRefId BIGINT NULL 
, name VARCHAR(255) NULL 
, path VARCHAR(255) NULL 
, mimeType VARCHAR(255) NULL 
, size DECIMAL(18) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE firm 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE meeting 
( 
  id BIGINT NOT NULL 
, type INT NULL COMMENT 'period, proje, mini' 
, project BIGINT NULL 
, name VARCHAR(255) NULL 
, subject VARCHAR(255) NULL 
, owner BIGINT NULL 
, period VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE menu 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, path VARCHAR(255) NULL 
, icon VARCHAR(255) NULL 
, parent BIGINT(18) NULL 
, role BIGINT NULL 
, ordering INT NULL 
, visible INT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT(18) NULL 
, updater BIGINT(18) NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE message 
( 
  id BIGINT NOT NULL 
, fromEntityType VARCHAR(50) NOT NULL COMMENT 'user, customer, customerContact' 
, fromEntityRefId BIGINT NULL COMMENT 'ilgili tipin referans id si' 
, fromAddress VARCHAR(255) NOT NULL COMMENT 'email address, GSM no' 
, msgSubject VARCHAR(511) NULL 
, msgBody TEXT NULL 
, msgType INT NOT NULL COMMENT '1:actionReport, 2:email, 3:message(ATMS)' 
, msgStatus INT NULL 
, attached INT NULL DEFAULT 0 
, relatedEntityType VARCHAR(50) NULL COMMENT 'activity, activityLog' 
, relatedEntityRefId BIGINT NULL COMMENT 'ilgili tipin referans id si' 
, timeSent DATETIME NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL COMMENT 'aktif, pasif' 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE messageRecepient 
( 
  id BIGINT NOT NULL 
, message BIGINT NOT NULL 
, msgType INT NOT NULL COMMENT '1:actionReport, 2:email, 3:message(ATMS)' 
, toEntityType VARCHAR(50) NOT NULL COMMENT 'user, customer, customerContact' 
, toEntityRefId BIGINT NULL COMMENT 'ilgili tipin referans id si' 
, toType VARCHAR(10) NULL COMMENT 'to, cc, bcc' 
, toAddress VARCHAR(255) NOT NULL COMMENT 'email address, gsm no' 
, timeViewed DATETIME NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL COMMENT 'aktif, pasif' 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE nonWorking 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, startDate DATETIME NULL 
, endDate DATETIME NULL 
, type INTEGER NULL 
, publish INT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT(18) NULL 
, updater BIGINT(18) NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE parameter 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NOT NULL 
, valueType VARCHAR(50) NULL 
, value VARCHAR(500) NOT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL COMMENT 'aktif, pasif' 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id )
, CONSTRAINT parameterInst_UK1 UNIQUE KEY ( name ) );

CREATE TABLE parameterSystem 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NOT NULL 
, value VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL COMMENT 'aktif, pasif' 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id )
, CONSTRAINT parameterSystem_UK1 UNIQUE KEY ( name ) );

CREATE TABLE permission 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL COMMENT '?rn:my.jsp' 
, descr VARCHAR(255) NULL 
, permLevel INT NOT NULL DEFAULT 0 COMMENT '0000:nothing
0001:view
0010:edit
0100:delete
1000:create' 
, enabled VARCHAR(1024) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE person 
( 
  id BIGINT NOT NULL 
, firstName VARCHAR(255) NULL 
, lastName VARCHAR(255) NULL 
, gender INT NULL 
, birthDay DATETIME NULL 
, refId VARCHAR(255) NULL 
, defaultUser BIGINT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE project 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL 
, purpose VARCHAR(255) NULL 
, leader BIGINT NULL 
, deliveryDate DATETIME NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE resource 
( 
  id BIGINT NULL 
, i18n_key VARCHAR(255) NULL 
, i18n_value VARCHAR(1024) NULL 
, i18n_locale VARCHAR(20) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL );

CREATE TABLE role 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE roleMenu 
( 
  id BIGINT NOT NULL 
, role BIGINT NULL 
, menu BIGINT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT(18) NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE rolePermission 
( 
  id BIGINT NOT NULL 
, role BIGINT NOT NULL 
, permission BIGINT NOT NULL 
, permLevel INT NULL COMMENT 'can not be greater than permission.permLevel' 
, enabled VARCHAR(1024) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE task 
( 
  id BIGINT NOT NULL 
, entityType VARCHAR(255) NULL 
, entityRefId BIGINT NULL 
, name VARCHAR(255) NULL 
, assignee BIGINT NULL 
, delivery DATETIME NULL 
, creator BIGINT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE user 
( 
  id BIGINT NOT NULL 
, userName VARCHAR(255) NOT NULL 
, password VARCHAR(255) NULL 
, department BIGINT NOT NULL 
, firstName VARCHAR(255) NULL 
, lastName VARCHAR(255) NULL 
, jobTitle VARCHAR(255) NULL 
, photoPath VARCHAR(255) NULL 
, email VARCHAR(255) NULL 
, tel VARCHAR(255) NULL 
, gsm VARCHAR(255) NULL 
, person BIGINT NULL 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL COMMENT 'aktif/pasif/izinde' 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userCustomer 
( 
  id BIGINT NOT NULL 
, user BIGINT NOT NULL 
, customer BIGINT NOT NULL 
, permLevel INT NOT NULL DEFAULT 255 
, refId VARCHAR(100) NULL 
, userCustomerType VARCHAR(50) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userFirm 
( 
  id BIGINT NOT NULL 
, user BIGINT NOT NULL 
, firm BIGINT NOT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id )
, CONSTRAINT userFirm_UK1 UNIQUE KEY ( user, firm ) );

CREATE TABLE userGroup 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, permLevel INT NOT NULL DEFAULT 0 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userPermission 
( 
  id BIGINT NOT NULL 
, user BIGINT NOT NULL 
, permission BIGINT NOT NULL 
, permLevel INT NOT NULL DEFAULT 0 COMMENT '0000:nothing
0001:view
0010:edit
0100:delete
1000:create' 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userRole 
( 
  id BIGINT NOT NULL 
, user BIGINT NULL 
, role BIGINT NULL 
, roleName VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userUser 
( 
  id BIGINT NOT NULL 
, user BIGINT NOT NULL 
, targetUser BIGINT NOT NULL 
, parent INT NOT NULL DEFAULT 0 COMMENT '1:firstParent
2:secondParent
.
.
n:nthParent' 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

ALTER TABLE activity ADD CONSTRAINT activity_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activity ADD CONSTRAINT activity_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activity ADD CONSTRAINT activity_FK3 FOREIGN KEY ( assigner )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activity ADD CONSTRAINT activity_FK4 FOREIGN KEY ( assignee )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activityLog ADD CONSTRAINT activityLog_FK1 FOREIGN KEY ( activity )
 REFERENCES activity ( id )
 ON DELETE RESTRICT;

ALTER TABLE activityLog ADD CONSTRAINT activityLog_FK2 FOREIGN KEY ( logger )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activityLog ADD CONSTRAINT activityLog_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activityLog ADD CONSTRAINT activityLog_FK4 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE association ADD CONSTRAINT association_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE association ADD CONSTRAINT association_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE comment ADD CONSTRAINT comment_FK1 FOREIGN KEY ( activity )
 REFERENCES activity ( id )
 ON DELETE RESTRICT;

ALTER TABLE comment ADD CONSTRAINT comment_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE comment ADD CONSTRAINT comment_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE customer ADD CONSTRAINT customer_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE customer ADD CONSTRAINT customer_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE customerContact ADD CONSTRAINT customerContact_FK1 FOREIGN KEY ( customer )
 REFERENCES customer ( id )
 ON DELETE RESTRICT;

ALTER TABLE customerContact ADD CONSTRAINT customerContact_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE customerContact ADD CONSTRAINT customerContact_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE department ADD CONSTRAINT department_FK1 FOREIGN KEY ( firm )
 REFERENCES firm ( id )
 ON DELETE RESTRICT;

ALTER TABLE department ADD CONSTRAINT department_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE department ADD CONSTRAINT department_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE fileAttachment ADD CONSTRAINT fileAttachment_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE fileAttachment ADD CONSTRAINT fileAttachment_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE firm ADD CONSTRAINT firm_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE firm ADD CONSTRAINT firm_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE meeting ADD CONSTRAINT meeting_FK1 FOREIGN KEY ( project )
 REFERENCES project ( id )
 ON DELETE RESTRICT;

ALTER TABLE meeting ADD CONSTRAINT meeting_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE meeting ADD CONSTRAINT meeting_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE menu ADD CONSTRAINT menu_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE menu ADD CONSTRAINT menu_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE menu ADD CONSTRAINT menu_FK3 FOREIGN KEY ( role )
 REFERENCES role ( id )
 ON DELETE RESTRICT;

ALTER TABLE message ADD CONSTRAINT message_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE message ADD CONSTRAINT message_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE messageRecepient ADD CONSTRAINT messageRecepient_FK1 FOREIGN KEY ( message )
 REFERENCES message ( id )
 ON DELETE RESTRICT;

ALTER TABLE messageRecepient ADD CONSTRAINT messageRecepient_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE messageRecepient ADD CONSTRAINT messageRecepient_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE nonWorking ADD CONSTRAINT nonWorking_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE nonWorking ADD CONSTRAINT nonWorking_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE parameter ADD CONSTRAINT parameter_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE parameter ADD CONSTRAINT parameter_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE parameterSystem ADD CONSTRAINT parameterSystem_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE parameterSystem ADD CONSTRAINT parameterSystem_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE permission ADD CONSTRAINT permission_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE permission ADD CONSTRAINT permission_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE person ADD CONSTRAINT person_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE person ADD CONSTRAINT person_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE project ADD CONSTRAINT project_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE project ADD CONSTRAINT project_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE resource ADD CONSTRAINT resource_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE resource ADD CONSTRAINT resource_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE role ADD CONSTRAINT role_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE role ADD CONSTRAINT role_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE roleMenu ADD CONSTRAINT roleMenu_FK1 FOREIGN KEY ( menu )
 REFERENCES menu ( id )
 ON DELETE RESTRICT;

ALTER TABLE roleMenu ADD CONSTRAINT roleMenu_FK2 FOREIGN KEY ( role )
 REFERENCES role ( id )
 ON DELETE RESTRICT;

ALTER TABLE roleMenu ADD CONSTRAINT roleMenu_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE roleMenu ADD CONSTRAINT roleMenu_FK4 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE rolePermission ADD CONSTRAINT rolePermission_FK1 FOREIGN KEY ( role )
 REFERENCES role ( id )
 ON DELETE RESTRICT;

ALTER TABLE rolePermission ADD CONSTRAINT rolePermission_FK2 FOREIGN KEY ( permission )
 REFERENCES permission ( id )
 ON DELETE RESTRICT;

ALTER TABLE rolePermission ADD CONSTRAINT rolePermission_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE rolePermission ADD CONSTRAINT rolePermission_FK4 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE task ADD CONSTRAINT task_FK1 FOREIGN KEY ( assignee )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE task ADD CONSTRAINT task_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE task ADD CONSTRAINT task_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE user ADD CONSTRAINT user_FK1 FOREIGN KEY ( department )
 REFERENCES department ( id )
 ON DELETE RESTRICT;

ALTER TABLE user ADD CONSTRAINT user_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE user ADD CONSTRAINT user_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE user ADD CONSTRAINT user_FK4 FOREIGN KEY ( person )
 REFERENCES person ( id )
 ON DELETE RESTRICT;

ALTER TABLE userCustomer ADD CONSTRAINT userCustomer_FK1 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userCustomer ADD CONSTRAINT userCustomer_FK2 FOREIGN KEY ( customer )
 REFERENCES customer ( id )
 ON DELETE RESTRICT;

ALTER TABLE userFirm ADD CONSTRAINT userFirm_FK1 FOREIGN KEY ( firm )
 REFERENCES firm ( id )
 ON DELETE RESTRICT;

ALTER TABLE userFirm ADD CONSTRAINT userFirm_FK2 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userFirm ADD CONSTRAINT userFirm_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userFirm ADD CONSTRAINT userFirm_FK4 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userGroup ADD CONSTRAINT userGroup_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userGroup ADD CONSTRAINT userGroup_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userPermission ADD CONSTRAINT userPermission_FK1 FOREIGN KEY ( permission )
 REFERENCES permission ( id )
 ON DELETE RESTRICT;

ALTER TABLE userPermission ADD CONSTRAINT userPermission_FK2 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userPermission ADD CONSTRAINT userPermission_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userPermission ADD CONSTRAINT userPermission_FK4 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userRole ADD CONSTRAINT userRole_FK1 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userRole ADD CONSTRAINT userRole_FK2 FOREIGN KEY ( role )
 REFERENCES role ( id )
 ON DELETE RESTRICT;

ALTER TABLE userRole ADD CONSTRAINT userRole_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userRole ADD CONSTRAINT userRole_FK4 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userUser ADD CONSTRAINT userUser_FK1 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userUser ADD CONSTRAINT userUser_FK2 FOREIGN KEY ( targetUser )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

CREATE INDEX activity_IX_STATUS ON activity ( status);

CREATE INDEX activityLog_IX_ACTIVITY_LOGGER ON activityLog ( activity, logger);

CREATE INDEX activityLog_IX_STATUS ON activityLog ( status);

CREATE INDEX association_IX_ENTITY_ALL ON association ( entityType, entityRefId, targetEntityType, targetEntityRefId);

CREATE INDEX association_IX_STATUS ON association ( id);

CREATE INDEX association_IX_TARGETENTITY ON association ( entityType, entityRefId);

CREATE INDEX comment_IX_STATUS ON comment ( status);

CREATE INDEX customer_IX_NAME ON customer ( name);

CREATE INDEX customer_IX_REFID ON customer ( refId);

CREATE INDEX customer_IX_STATUS ON customer ( status);

CREATE INDEX customerContact_CUSTOMER ON customerContact ( customer);

CREATE INDEX customerContact_IX_REFID ON customerContact ( refId);

CREATE INDEX customerContact_IX_STATUS ON customerContact ( status);

CREATE INDEX department_IX_NAME ON department ( name);

CREATE INDEX department_IX_REFID ON department ( refId);

CREATE INDEX department_IX_STATUS ON department ( status);

CREATE INDEX fileAttachment_IX_ENTITY ON fileAttachment ( entityType, entityRefId);

CREATE INDEX fileAttachment_IX_STATUS ON fileAttachment ( status);

CREATE INDEX firm_IX_REFID ON firm ( refId);

CREATE INDEX firm_IX_STATUS ON firm ( status);

CREATE INDEX meeting_IX_STATUS ON meeting ( status);

CREATE INDEX menu_IX_STATUS ON menu ( status);

CREATE INDEX nonWorking_IX_STATUS ON nonWorking ( status);

CREATE INDEX parameter_IX_NAME ON parameter ( name);

CREATE INDEX parameter_IX_STATUS ON parameter ( status);

CREATE INDEX parameterSystem_IX_NAME ON parameterSystem ( name);

CREATE INDEX parameterSystem_IX_STATUS ON parameterSystem ( status);

CREATE INDEX permission_IX_NAME ON permission ( name);

CREATE INDEX Person_IX_STATUS ON person ( status);

CREATE INDEX project_IX_STATUS ON project ( status);

CREATE INDEX resource_IX_KEY ON resource ( i18n_key);

CREATE INDEX resource_IX_LOCALE_KEY ON resource ( i18n_locale, i18n_key);

CREATE INDEX role_IX_NAME ON role ( name);

CREATE INDEX roleMenu_IX_STATUS ON roleMenu ( status);

CREATE INDEX rolePermission_IX_ROLE_PERM ON rolePermission ( role, permission);

CREATE INDEX rolePermission_IX_STATUS ON rolePermission ( status);

CREATE INDEX user_DEPARTMENT_UN ON user ( department, userName);

CREATE INDEX user_IX_REFID ON user ( refId);

CREATE INDEX user_IX_STATUS ON user ( status);

CREATE INDEX user_USERNAME ON user ( userName);

CREATE INDEX userCustomer_IX_CUSTOMER_USER ON userCustomer ( customer, user);

CREATE INDEX userCustomer_IX_REFID ON userCustomer ( refId);

CREATE INDEX userCustomer_IX_STATUS ON userCustomer ( status);

CREATE INDEX userCustomer_IX_USER ON userCustomer ( user);

CREATE INDEX userFirm_IX_FIRM_USER ON userFirm ( firm, user);

CREATE INDEX userPermission_IX_PERM ON userPermission ( permission);

CREATE INDEX userPermission_IX_USER ON userPermission ( user);

CREATE INDEX userRole_IX_STATUS ON userRole ( status);

CREATE INDEX userRole_IX_USER ON userRole ( user);

CREATE INDEX userUser_IX_REFID ON userUser ( refId);

CREATE INDEX userUser_IX_STATUS ON userUser ( status);

CREATE INDEX userUser_IX_USER_TARGETU ON userUser ( user, targetUser);
