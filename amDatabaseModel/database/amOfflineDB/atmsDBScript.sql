CREATE TABLE activity 
( 
  id DECIMAL(18) NOT NULL 
, planStartDate DATETIME NULL 
, planEndDate DATETIME NULL 
, title VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, resolution INT NULL 
, assignee DECIMAL(18) NULL 
, version VARCHAR(50) NULL 
, entityType VARCHAR(255) NULL COMMENT 'customer veya meeting ' 
, entityRefId DECIMAL(18) NULL COMMENT 'ilgili tipin referans id si' 
, type INT NULL 
, important INT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE activityLog 
( 
  id DECIMAL(18) NOT NULL 
, descr TEXT NULL 
, important INT NULL 
, activityId DECIMAL(18) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE association 
( 
  id DECIMAL(18) NOT NULL 
, entityType VARCHAR(255) NULL 
, entityRefId DECIMAL(18) NULL 
, targetEntityType VARCHAR(255) NULL 
, targetEntityRefId DECIMAL(18) NULL 
, association VARCHAR(50) NOT NULL COMMENT 'participates, likes' 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE comment 
( 
  id DECIMAL(18) NOT NULL 
, activityId DECIMAL(18) NULL 
, desc VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE customer 
( 
  id DECIMAL(18) NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NULL 
, tel VARCHAR(20) NULL 
, fax VARCHAR(20) NULL 
, email VARCHAR(100) NULL 
, class VARCHAR(255) NULL 
, sector VARCHAR(255) NULL 
, refId VARCHAR(100) NULL 
, type INT NOT NULL DEFAULT 1 
, address VARCHAR(255) NULL 
, district VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, id1 DECIMAL(18) NOT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE customerContact 
( 
  id DECIMAL(18) NOT NULL 
, customerId DECIMAL(18) NOT NULL 
, firstName VARCHAR(255) NOT NULL 
, lastName VARCHAR(255) NOT NULL 
, tel VARCHAR(255) NULL 
, fax VARCHAR(255) NULL 
, gsm VARCHAR(255) NULL 
, email VARCHAR(255) NULL 
, locationId INT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE customerLocation 
( 
  id DECIMAL(18) NOT NULL 
, customerId DECIMAL(18) NOT NULL 
, address VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE department 
( 
  id DECIMAL(18) NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NULL 
, firmId DECIMAL(18) NOT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE fileAttachment 
( 
  id DECIMAL(18) NOT NULL 
, activityId DECIMAL(18) NULL 
, name VARCHAR(255) NULL 
, path VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE firm 
( 
  id DECIMAL(18) NOT NULL 
, name VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE meeting 
( 
  id DECIMAL(18) NOT NULL 
, type INT NULL COMMENT 'period, proje, mini' 
, projectId DECIMAL(18) NULL 
, name VARCHAR(255) NULL 
, subject VARCHAR(255) NULL 
, owner DECIMAL(18) NULL 
, period VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE parameter 
( 
  id DECIMAL(18) NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NOT NULL 
, value VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL COMMENT 'aktif, pasif' 
, CONSTRAINT PRIMARY KEY ( id )
, CONSTRAINT parameterInst_UK1 UNIQUE KEY ( name ) );

CREATE TABLE parameterSystem 
( 
  id DECIMAL(18) NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NOT NULL 
, value VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL COMMENT 'aktif, pasif' 
, CONSTRAINT PRIMARY KEY ( id )
, CONSTRAINT parameterSystem_UK1 UNIQUE KEY ( name ) );

CREATE TABLE permission 
( 
  id DECIMAL(18) NOT NULL 
, name VARCHAR(255) NULL COMMENT '�rn:my.jsp' 
, descr VARCHAR(255) NULL 
, permLevel INT NOT NULL DEFAULT 0 COMMENT '0000:nothing
0001:view
0010:edit
0100:delete
1000:create' 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updaer DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE project 
( 
  id DECIMAL(18) NOT NULL 
, name VARCHAR(255) NULL 
, purpose VARCHAR(255) NULL 
, leader DECIMAL(18) NULL 
, deliveryDate DATETIME NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE user 
( 
  id DECIMAL(18) NOT NULL 
, userName VARCHAR(255) NOT NULL 
, departmentId DECIMAL(18) NOT NULL 
, firstName VARCHAR(255) NULL 
, lastName VARCHAR(255) NULL 
, created DATETIME NULL 
, updated VARCHAR(255) NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL COMMENT 'aktif/pasif/izinde' 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userCustomer 
( 
  id DECIMAL(18) NOT NULL 
, userId DECIMAL(18) NOT NULL 
, customerId DECIMAL(18) NOT NULL 
, permLevel INT NOT NULL DEFAULT 255 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userFirm 
( 
  id DECIMAL(18) NOT NULL 
, userId DECIMAL(18) NOT NULL 
, firmId DECIMAL(18) NOT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id )
, CONSTRAINT userFirm_UK1 UNIQUE KEY ( userId, firmId ) );

CREATE TABLE userGroup 
( 
  id DECIMAL(18) NOT NULL 
, name VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, permLevel INT NOT NULL DEFAULT 0 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userPermission 
( 
  id DECIMAL(18) NOT NULL 
, userId DECIMAL(18) NOT NULL 
, permId DECIMAL(18) NOT NULL 
, permLevel INT NOT NULL DEFAULT 0 COMMENT '0000:nothing
0001:view
0010:edit
0100:delete
1000:create' 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, status INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userUser 
( 
  id DECIMAL(18) NOT NULL 
, userId DECIMAL(18) NOT NULL 
, targetUserId DECIMAL(18) NOT NULL 
, parent INT NOT NULL DEFAULT 0 COMMENT '1:firstParent
2:secondParent
.
.
n:nthParent' 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator DECIMAL(18) NULL 
, updater DECIMAL(18) NULL 
, user INT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

ALTER TABLE activityLog ADD CONSTRAINT activityLog_FK1 FOREIGN KEY ( activityId )
 REFERENCES activity ( id )
 ON DELETE RESTRICT;

ALTER TABLE comment ADD CONSTRAINT comment_FK1 FOREIGN KEY ( activityId )
 REFERENCES activity ( id )
 ON DELETE RESTRICT;

ALTER TABLE customerContact ADD CONSTRAINT customerContact_FK1 FOREIGN KEY ( customerId )
 REFERENCES customer ( id )
 ON DELETE RESTRICT;

ALTER TABLE customerLocation ADD CONSTRAINT customerLocation_FK1 FOREIGN KEY ( customerId )
 REFERENCES customer ( id )
 ON DELETE RESTRICT;

ALTER TABLE department ADD CONSTRAINT department_FK1 FOREIGN KEY ( firmId )
 REFERENCES firm ( id )
 ON DELETE RESTRICT;

ALTER TABLE fileAttachment ADD CONSTRAINT fileAttachment_FK1 FOREIGN KEY ( activityId )
 REFERENCES activity ( id )
 ON DELETE RESTRICT;

ALTER TABLE meeting ADD CONSTRAINT meeting_FK1 FOREIGN KEY ( projectId )
 REFERENCES project ( id )
 ON DELETE RESTRICT;

ALTER TABLE user ADD CONSTRAINT user_FK1 FOREIGN KEY ( departmentId )
 REFERENCES department ( id )
 ON DELETE RESTRICT;

ALTER TABLE userCustomer ADD CONSTRAINT userCustomer_FK1 FOREIGN KEY ( userId )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userCustomer ADD CONSTRAINT userCustomer_FK2 FOREIGN KEY ( customerId )
 REFERENCES customer ( id )
 ON DELETE RESTRICT;

ALTER TABLE userFirm ADD CONSTRAINT userFirm_FK1 FOREIGN KEY ( firmId )
 REFERENCES firm ( id )
 ON DELETE RESTRICT;

ALTER TABLE userFirm ADD CONSTRAINT userFirm_FK2 FOREIGN KEY ( userId )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userPermission ADD CONSTRAINT userPermission_FK1 FOREIGN KEY ( permId )
 REFERENCES permission ( id )
 ON DELETE RESTRICT;

ALTER TABLE userPermission ADD CONSTRAINT userPermission_FK2 FOREIGN KEY ( userId )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userUser ADD CONSTRAINT userUser_FK1 FOREIGN KEY ( userId )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userUser ADD CONSTRAINT userUser_FK2 FOREIGN KEY ( targetUserId )
 REFERENCES user ( id )
 ON DELETE RESTRICT;
