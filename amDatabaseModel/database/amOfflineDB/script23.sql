CREATE TABLE activity 
( 
  id BIGINT(18) NOT NULL 
, planStartDate DATETIME NULL 
, estimated BIGINT NULL COMMENT 'estimated(in minitues)=planEndDate - planStartDate' 
, timeSpent BIGINT NULL 
, planEndDate DATETIME NULL 
, title VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, resolution INT NULL COMMENT '1=realized,
2=partially_realized,
3=not_realized' 
, assignee BIGINT NULL 
, assigner BIGINT NULL 
, version VARCHAR(50) NULL 
, entityType VARCHAR(255) NULL COMMENT 'customer veya meeting ' 
, entityRefId BIGINT NULL COMMENT 'ilgili tipin referans id si' 
, entityDisplayName VARCHAR(255) NULL 
, type INT NULL 
, important INT NULL 
, activityStatus VARCHAR(255) NULL COMMENT '1=open,
2=reopened,
3=closed' 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE activityLog 
( 
  id BIGINT NOT NULL 
, descr TEXT NULL 
, important INT NULL 
, activity BIGINT NULL 
, logDate DATETIME NULL 
, logger BIGINT NULL 
, activityLogStatus INT NULL COMMENT '1=realized,
2=not_realized' 
, timeSpent INT NULL COMMENT 'in minutes' 
, reason INT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE association 
( 
  id BIGINT NOT NULL 
, entityType VARCHAR(255) NULL 
, entityRefId BIGINT NULL 
, targetEntityType VARCHAR(255) NULL 
, targetEntityRefId BIGINT NULL 
, association VARCHAR(50) NOT NULL COMMENT 'participates, likes' 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE comment 
( 
  id BIGINT NOT NULL 
, activity BIGINT NULL 
, descr VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE customer 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NULL 
, tel VARCHAR(20) NULL 
, fax VARCHAR(20) NULL 
, email VARCHAR(100) NULL 
, class VARCHAR(255) NULL 
, sector VARCHAR(255) NULL 
, type INT NOT NULL DEFAULT 1 
, address VARCHAR(255) NULL 
, district VARCHAR(255) NULL 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE customerContact 
( 
  id BIGINT NOT NULL 
, customer BIGINT NOT NULL 
, firstName VARCHAR(255) NOT NULL 
, lastName VARCHAR(255) NOT NULL 
, jobTitle VARCHAR(255) NULL 
, tel VARCHAR(255) NULL 
, fax VARCHAR(255) NULL 
, gsm VARCHAR(255) NULL 
, email VARCHAR(255) NULL 
, locationId INT NULL 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, primaryContact INT NULL DEFAULT 0 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE department 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NULL 
, firm BIGINT NOT NULL 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE fileAttachment 
( 
  id BIGINT NOT NULL 
, entityType VARCHAR(255) NULL 
, entityRefId BIGINT NULL 
, name VARCHAR(255) NULL 
, path VARCHAR(255) NULL 
, mimeType VARCHAR(255) NULL 
, size DECIMAL(18) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE firm 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE meeting 
( 
  id BIGINT NOT NULL 
, type INT NULL COMMENT 'period, proje, mini' 
, project BIGINT NULL 
, name VARCHAR(255) NULL 
, subject VARCHAR(255) NULL 
, owner BIGINT NULL 
, period VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE nonWorking 
( 
  id BIGINT(18) NOT NULL 
, name VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, startDate DATETIME NULL 
, endDate DATETIME NULL 
, type INTEGER NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT(18) NULL 
, updater BIGINT(18) NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE parameter 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NOT NULL 
, valueType VARCHAR(50) NULL 
, value VARCHAR(500) NOT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL COMMENT 'aktif, pasif' 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id )
, CONSTRAINT parameterInst_UK1 UNIQUE KEY ( name ) );

CREATE TABLE parameterSystem 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NOT NULL 
, descr VARCHAR(255) NOT NULL 
, value VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL COMMENT 'aktif, pasif' 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id )
, CONSTRAINT parameterSystem_UK1 UNIQUE KEY ( name ) );

CREATE TABLE permission 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL COMMENT '�rn:my.jsp' 
, descr VARCHAR(255) NULL 
, permLevel INT NOT NULL DEFAULT 0 COMMENT '0000:nothing
0001:view
0010:edit
0100:delete
1000:create' 
, enabled VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE person 
( 
  id BIGINT NOT NULL 
, firstName VARCHAR(255) NULL 
, lastName VARCHAR(255) NULL 
, gender INT NULL 
, birthDay DATETIME NULL 
, user BIGINT NULL 
, refId VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE project 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL 
, purpose VARCHAR(255) NULL 
, leader BIGINT NULL 
, deliveryDate DATETIME NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE role 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE rolePermission 
( 
  id BIGINT NOT NULL 
, role BIGINT NOT NULL 
, permission BIGINT NOT NULL 
, permLevel INT NULL COMMENT 'can not be greater than permission.permLevel' 
, enabled VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE task 
( 
  id BIGINT NOT NULL 
, entityType VARCHAR(255) NULL 
, entityRefId BIGINT NULL 
, name VARCHAR(255) NULL 
, assignee BIGINT NULL 
, delivery DATETIME NULL 
, creator BIGINT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE user 
( 
  id BIGINT NOT NULL 
, userName VARCHAR(255) NOT NULL 
, password VARCHAR(255) NULL 
, department BIGINT NOT NULL 
, firstName VARCHAR(255) NULL 
, lastName VARCHAR(255) NULL 
, jobTitle VARCHAR(255) NULL 
, photoPath VARCHAR(255) NULL 
, email VARCHAR(255) NULL 
, tel VARCHAR(255) NULL 
, gsm VARCHAR(255) NULL 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL COMMENT 'aktif/pasif/izinde' 
, person BIGINT NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userCustomer 
( 
  id BIGINT NOT NULL 
, user BIGINT NOT NULL 
, customer BIGINT NOT NULL 
, permLevel INT NOT NULL DEFAULT 255 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userFirm 
( 
  id BIGINT NOT NULL 
, user BIGINT NOT NULL 
, firm BIGINT NOT NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id )
, CONSTRAINT userFirm_UK1 UNIQUE KEY ( user, firm ) );

CREATE TABLE userGroup 
( 
  id BIGINT NOT NULL 
, name VARCHAR(255) NULL 
, descr VARCHAR(255) NULL 
, permLevel INT NOT NULL DEFAULT 0 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userPermission 
( 
  id BIGINT NOT NULL 
, user BIGINT NOT NULL 
, permission BIGINT NOT NULL 
, permLevel INT NOT NULL DEFAULT 0 COMMENT '0000:nothing
0001:view
0010:edit
0100:delete
1000:create' 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userRole 
( 
  id BIGINT NOT NULL 
, user BIGINT NULL 
, role BIGINT NULL 
, roleName VARCHAR(255) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

CREATE TABLE userUser 
( 
  id BIGINT NOT NULL 
, user BIGINT NOT NULL 
, targetUser BIGINT NOT NULL 
, parent INT NOT NULL DEFAULT 0 COMMENT '1:firstParent
2:secondParent
.
.
n:nthParent' 
, refId VARCHAR(100) NULL 
, created DATETIME NULL 
, updated DATETIME NULL 
, creator BIGINT NULL 
, updater BIGINT NULL 
, status INT NULL 
, statusDescr VARCHAR(255) NULL 
, CONSTRAINT PRIMARY KEY ( id ) );

ALTER TABLE activity ADD CONSTRAINT activity_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activity ADD CONSTRAINT activity_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activity ADD CONSTRAINT activity_FK3 FOREIGN KEY ( assigner )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activity ADD CONSTRAINT activity_FK4 FOREIGN KEY ( assignee )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activityLog ADD CONSTRAINT activityLog_FK1 FOREIGN KEY ( activity )
 REFERENCES activity ( id )
 ON DELETE RESTRICT;

ALTER TABLE activityLog ADD CONSTRAINT activityLog_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activityLog ADD CONSTRAINT activityLog_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE activityLog ADD CONSTRAINT activityLog_FK4 FOREIGN KEY ( logger )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE association ADD CONSTRAINT association_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE association ADD CONSTRAINT association_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE comment ADD CONSTRAINT comment_FK1 FOREIGN KEY ( activity )
 REFERENCES activity ( id )
 ON DELETE RESTRICT;

ALTER TABLE comment ADD CONSTRAINT comment_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE comment ADD CONSTRAINT comment_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE customer ADD CONSTRAINT customer_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE customer ADD CONSTRAINT customer_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE customerContact ADD CONSTRAINT customerContact_FK1 FOREIGN KEY ( customer )
 REFERENCES customer ( id )
 ON DELETE RESTRICT;

ALTER TABLE customerContact ADD CONSTRAINT customerContact_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE customerContact ADD CONSTRAINT customerContact_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE department ADD CONSTRAINT department_FK1 FOREIGN KEY ( firm )
 REFERENCES firm ( id )
 ON DELETE RESTRICT;

ALTER TABLE department ADD CONSTRAINT department_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE department ADD CONSTRAINT department_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE fileAttachment ADD CONSTRAINT fileAttachment_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE fileAttachment ADD CONSTRAINT fileAttachment_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE firm ADD CONSTRAINT firm_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE firm ADD CONSTRAINT firm_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE meeting ADD CONSTRAINT meeting_FK1 FOREIGN KEY ( project )
 REFERENCES project ( id )
 ON DELETE RESTRICT;

ALTER TABLE meeting ADD CONSTRAINT meeting_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE meeting ADD CONSTRAINT meeting_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE nonWorking ADD CONSTRAINT nonWorking_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE nonWorking ADD CONSTRAINT nonWorking_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE parameter ADD CONSTRAINT parameter_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE parameter ADD CONSTRAINT parameter_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE parameterSystem ADD CONSTRAINT parameterSystem_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE parameterSystem ADD CONSTRAINT parameterSystem_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE permission ADD CONSTRAINT permission_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE permission ADD CONSTRAINT permission_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE person ADD CONSTRAINT Person_FK1 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE person ADD CONSTRAINT Person_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE person ADD CONSTRAINT Person_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE project ADD CONSTRAINT project_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE project ADD CONSTRAINT project_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE role ADD CONSTRAINT role_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE role ADD CONSTRAINT role_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE rolePermission ADD CONSTRAINT rolePermission_FK1 FOREIGN KEY ( role )
 REFERENCES role ( id )
 ON DELETE RESTRICT;

ALTER TABLE rolePermission ADD CONSTRAINT rolePermission_FK2 FOREIGN KEY ( permission )
 REFERENCES permission ( id )
 ON DELETE RESTRICT;

ALTER TABLE rolePermission ADD CONSTRAINT rolePermission_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE rolePermission ADD CONSTRAINT rolePermission_FK4 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE task ADD CONSTRAINT task_FK1 FOREIGN KEY ( assignee )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE task ADD CONSTRAINT task_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE task ADD CONSTRAINT task_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE user ADD CONSTRAINT user_FK1 FOREIGN KEY ( department )
 REFERENCES department ( id )
 ON DELETE RESTRICT;

ALTER TABLE user ADD CONSTRAINT user_FK2 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE user ADD CONSTRAINT user_FK3 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE user ADD CONSTRAINT user_FK4 FOREIGN KEY ( person )
 REFERENCES person ( id )
 ON DELETE RESTRICT;

ALTER TABLE userCustomer ADD CONSTRAINT userCustomer_FK1 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userCustomer ADD CONSTRAINT userCustomer_FK2 FOREIGN KEY ( customer )
 REFERENCES customer ( id )
 ON DELETE RESTRICT;

ALTER TABLE userFirm ADD CONSTRAINT userFirm_FK1 FOREIGN KEY ( firm )
 REFERENCES firm ( id )
 ON DELETE RESTRICT;

ALTER TABLE userFirm ADD CONSTRAINT userFirm_FK2 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userFirm ADD CONSTRAINT userFirm_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userFirm ADD CONSTRAINT userFirm_FK4 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userGroup ADD CONSTRAINT userGroup_FK1 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userGroup ADD CONSTRAINT userGroup_FK2 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userPermission ADD CONSTRAINT userPermission_FK1 FOREIGN KEY ( permission )
 REFERENCES permission ( id )
 ON DELETE RESTRICT;

ALTER TABLE userPermission ADD CONSTRAINT userPermission_FK2 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userPermission ADD CONSTRAINT userPermission_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userPermission ADD CONSTRAINT userPermission_FK4 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userRole ADD CONSTRAINT userRole_FK1 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userRole ADD CONSTRAINT userRole_FK2 FOREIGN KEY ( role )
 REFERENCES role ( id )
 ON DELETE RESTRICT;

ALTER TABLE userRole ADD CONSTRAINT userRole_FK3 FOREIGN KEY ( creator )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userRole ADD CONSTRAINT userRole_FK4 FOREIGN KEY ( updater )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userUser ADD CONSTRAINT userUser_FK1 FOREIGN KEY ( user )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

ALTER TABLE userUser ADD CONSTRAINT userUser_FK2 FOREIGN KEY ( targetUser )
 REFERENCES user ( id )
 ON DELETE RESTRICT;

CREATE INDEX activityLog_IX_ACTIVITY_LOGGER ON activityLog ( activity, logger);

CREATE INDEX association_IX_ENTITY_ALL ON association ( entityType, entityRefId, targetEntityType, targetEntityRefId);

CREATE INDEX association_IX_TARGETENTITY ON association ( entityType, entityRefId);

CREATE INDEX customer_IX_NAME ON customer ( name);

CREATE INDEX customer_IX_REFID ON customer ( refId);

CREATE INDEX customerContact_CUSTOMER ON customerContact ( customer);

CREATE INDEX customerContact_IX_REFID ON customerContact ( refId);

CREATE INDEX department_IX_NAME ON department ( name);

CREATE INDEX department_IX_REFID ON department ( refId);

CREATE INDEX fileAttachment_IX_ENTITY ON fileAttachment ( entityType, entityRefId);

CREATE INDEX firm_IX_REFID ON firm ( refId);

CREATE INDEX parameter_IX_NAME ON parameter ( name);

CREATE INDEX parameterSystem_IX_NAME ON parameterSystem ( name);

CREATE INDEX permission_IX_NAME ON permission ( name);

CREATE INDEX Person_INDEX1 ON person ( user);

CREATE INDEX Person_INDEX2 ON person ( status);

CREATE INDEX role_IX_NAME ON role ( name);

CREATE INDEX rolePermission_IX_ROLE_PERM ON rolePermission ( role, permission);

CREATE INDEX user_DEPARTMENT_UN ON user ( department, userName);

CREATE INDEX user_IX_REFID ON user ( refId);

CREATE INDEX user_USERNAME ON user ( userName);

CREATE INDEX userCustomer_IX_CUSTOMER_USER ON userCustomer ( customer, user);

CREATE INDEX userCustomer_IX_REFID ON userCustomer ( refId);

CREATE INDEX userCustomer_IX_USER ON userCustomer ( user);

CREATE INDEX userFirm_IX_FIRM_USER ON userFirm ( firm, user);

CREATE INDEX userPermission_IX_PERM ON userPermission ( permission);

CREATE INDEX userPermission_IX_USER ON userPermission ( user);

CREATE INDEX userRole_IX_USER ON userRole ( user);

CREATE INDEX userUser_IX_REFID ON userUser ( refId);

CREATE INDEX userUser_IX_USER_TARGETU ON userUser ( user, targetUser);
